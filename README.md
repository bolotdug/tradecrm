#       TradeCRM Application

## Entities

1. Client
2. Product
3. User
4. PurchaseInvoice
5. SalesDocument

## Functionality

1. CRUD operations for entities
2. Authorization & authentication
3. User interface
4. Operations to manage products