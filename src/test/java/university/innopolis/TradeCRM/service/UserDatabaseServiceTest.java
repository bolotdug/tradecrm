package university.innopolis.TradeCRM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.TradeCrmApplication;
import university.innopolis.TradeCRM.TradeCrmApplicationTests;
import university.innopolis.TradeCRM.models.User;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class UserDatabaseServiceTest extends TradeCrmApplicationTests {

    @Autowired
    private UserDatabaseService service;

    @Test
    void testGetAll() {
        List<User> userList = service.getAll();
        assertNotNull(userList);
    }

//    @Test
//    void testGetById() {
//        User createdUser = service.createUser(createTestUser());
//
//        User user = service.getById(createdUser.getId());
//
//        assertNotNull(user);
////        assertEquals(user.getId(), createdUser.getId());
////        assertEquals(user.getEmail(), createdUser.getEmail());
////        assertEquals(user.getFirstName(), createdUser.getFirstName());
////        assertEquals(user.getLastName(), createdUser.getLastName());
//    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"user with id %s not found\"", uuid), exception.getMessage());
    }

//    @Test
//    void testCreateUser() {
//        User testUser = createTestUser();
//
//        User createdUser = service.createUser(testUser);
//
//        assertNotNull(testUser);
//        assertEquals(testUser.getId(), createdUser.getId());
//        assertEquals(testUser.getEmail(), createdUser.getEmail());
//        assertEquals(testUser.getFirstName(), createdUser.getFirstName());
//        assertEquals(testUser.getLastName(), createdUser.getLastName());
//    }

//    @Test
//    void testUpdateUser() {
//        User createdUser = service.createUser(createTestUser());
//
//        User userToUpdate = new User(createdUser).toBuilder()
//                .id(UUID.randomUUID())
//                .firstName("Name1")
//                .lastName("Family1")
//                .email("builder@mai22l.ru")
////                .hashPassword("jkl")
////                .role(User.Role.USER)
//                .build();
//
//        User updatedUser = service.updateUser(createdUser.getId(), userToUpdate);
//
//        assertEquals(createdUser.getId(), updatedUser.getId(), "Ids shouldn't be changed during the update");
//        assertNotEquals(updatedUser.getFirstName(), createdUser.getFirstName());
//        assertNotEquals(updatedUser.getLastName(), createdUser.getLastName());
//        assertNotEquals(updatedUser.getEmail(), createdUser.getEmail());
//    }

//    @Test
//    void testDelete() {
//        User createdUser = service.createUser(createTestUser());
//        UUID createdUserId = createdUser.getId();
//        service.delete(createdUserId);
//
//        ResponseStatusException exception = assertThrows(
//                ResponseStatusException.class,
//                () -> service.getById(createdUserId),
//                format("Expected to return nothing and throw exception, since User with id %s was deleted",
//                        createdUserId)
//        );
//
//        assertEquals(format("404 NOT_FOUND \"user with id %s not found\"", createdUserId), exception.getMessage());
//
//    }



    private User createTestUser() {
        return User.builder()
                .firstName("Name")
                .lastName("Family")
                .email("builder@mail.ru")
//                .hashPassword("sota")
//                .role(User.Role.ADMIN)
                .build();
    }
}