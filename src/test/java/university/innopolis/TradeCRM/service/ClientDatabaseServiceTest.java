package university.innopolis.TradeCRM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.TradeCrmApplication;
import university.innopolis.TradeCRM.TradeCrmApplicationTests;
import university.innopolis.TradeCRM.models.Client;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ClientDatabaseServiceTest extends TradeCrmApplicationTests {

    @Autowired
    private ClientDatabaseService service;

    @Test
    void testGetAll() {
        List<Client> clientList = service.getAll();
        assertNotNull(clientList);
    }

    @Test
    void testGetById() {
        Client createdClient = service.createClient(createTestClient());

        Client client = service.getById(createdClient.getId());

        assertNotNull(client);
        assertEquals(client.getId(), createdClient.getId());
        assertEquals(client.getEmail(), createdClient.getEmail());
        assertEquals(client.getFirstName(), createdClient.getFirstName());
        assertEquals(client.getLastName(), createdClient.getLastName());
        assertEquals(client.getPhone(), createdClient.getPhone());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void testCreateClient() {
        Client testClient = createTestClient();

        Client createdClient = service.createClient(testClient);

        assertNotNull(testClient);
        assertEquals(testClient.getId(), createdClient.getId());
        assertEquals(testClient.getEmail(), createdClient.getEmail());
        assertEquals(testClient.getFirstName(), createdClient.getFirstName());
        assertEquals(testClient.getLastName(), createdClient.getLastName());
        assertEquals(testClient.getPhone(), createdClient.getPhone());

    }

    @Test
    void testUpdateClient() {
        Client createdClient = service.createClient(createTestClient());

        Client clientToUpdate = new Client(createdClient).toBuilder()
                .id(UUID.randomUUID())
                .firstName("Name1")
                .lastName("Family1")
                .phone("+791121051223")
                .email("builder@mai22l.ru")
                .build();

        Client updatedClient = service.updateClient(createdClient.getId(), clientToUpdate);

        assertEquals(createdClient.getId(), updatedClient.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(updatedClient.getFirstName(), createdClient.getFirstName());
        assertNotEquals(updatedClient.getLastName(), createdClient.getLastName());
        assertNotEquals(updatedClient.getEmail(), createdClient.getEmail());
        assertNotEquals(updatedClient.getPhone(), createdClient.getPhone());
    }

    @Test
    void testDelete() {
        Client createdClient = service.createClient(createTestClient());
        UUID createdClientId = createdClient.getId();
        service.delete(createdClientId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(createdClientId),
                format("Expected to return nothing and throw exception, since Client with id %s was deleted",
                        createdClientId)
        );

        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", createdClientId), exception.getMessage());

    }



    private Client createTestClient() {
        return Client.builder()
                .firstName("Name")
                .lastName("Family")
                .phone("+79112105123")
                .email("builder@mail.ru")
                .build();
    }
}