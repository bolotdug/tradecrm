package university.innopolis.TradeCRM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.TradeCrmApplicationTests;
import university.innopolis.TradeCRM.models.Product;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ProductDatabaseServiceTest extends TradeCrmApplicationTests {

    @Autowired
    private ProductDatabaseService service;

    @Test
    void testGetAll() {
        List<Product> ProductList = service.getAll();
        assertNotNull(ProductList);
    }

    @Test
    void testGetById() {
        Product createdProduct = service.createProduct(createTestProduct());

        Product Product = service.getById(createdProduct.getId());

        assertNotNull(Product);
        assertEquals(Product.getId(), createdProduct.getId());
        assertEquals(Product.getQuantity(), createdProduct.getQuantity());
        assertEquals(Product.getCostPrice(), createdProduct.getCostPrice());
        assertEquals(Product.getRetailPrice(), createdProduct.getRetailPrice());
        assertEquals(Product.getDescription(), createdProduct.getDescription());
        assertEquals(Product.getName(), createdProduct.getName());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"product with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void testCreateProduct() {
        Product testProduct = createTestProduct();

        Product createdProduct = service.createProduct(testProduct);

        assertNotNull(testProduct);
        assertEquals(testProduct.getId(), createdProduct.getId());
        assertEquals(testProduct.getQuantity(), createdProduct.getQuantity());
        assertEquals(testProduct.getCostPrice(), createdProduct.getCostPrice());
        assertEquals(testProduct.getRetailPrice(), createdProduct.getRetailPrice());
        assertEquals(testProduct.getDescription(), createdProduct.getDescription());
        assertEquals(testProduct.getName(), createdProduct.getName());

    }

    @Test
    void testUpdateProduct() {
        Product createdProduct = service.createProduct(createTestProduct());

        Product ProductToUpdate = new Product(createdProduct).toBuilder()
                .id(UUID.randomUUID())
                .name("Eraser1")
                .description("very good1")
                .costPrice(7)
                .retailPrice(110)
                .quantity(40)
                .build();

        Product updatedProduct = service.updateProduct(createdProduct.getId(), ProductToUpdate);

        assertEquals(createdProduct.getId(), updatedProduct.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(updatedProduct.getName(), createdProduct.getName());
        assertNotEquals(updatedProduct.getDescription(), createdProduct.getDescription());
        assertNotEquals(updatedProduct.getQuantity(), createdProduct.getQuantity());
        assertNotEquals(updatedProduct.getCostPrice(), createdProduct.getCostPrice());
    }

    @Test
    void testDelete() {
        Product createdProduct = service.createProduct(createTestProduct());
        UUID createdProductId = createdProduct.getId();
        service.delete(createdProductId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(createdProductId),
                format("Expected to return nothing and throw exception, since Product with id %s was deleted",
                        createdProductId)
        );

        assertEquals(format("404 NOT_FOUND \"product with id %s not found\"", createdProductId), exception.getMessage());

    }



    private Product createTestProduct() {
        return Product.builder()
                .name("Eraser")
                .description("very good")
                .costPrice(5)
                .retailPrice(10)
                .quantity(4)
                .build();
    }
}