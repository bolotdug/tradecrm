package university.innopolis.TradeCRM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.TradeCrmApplicationTests;
import university.innopolis.TradeCRM.models.Client;
import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.models.PurchaseInvoice;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class PurchaseInvoiceDatabaseServiceTest extends TradeCrmApplicationTests {

    @Autowired
    private PurchaseInvoiceDatabaseService service;

    @Test
    void testGetAll() {
        List<PurchaseInvoice> PurchaseInvoiceList = service.getAll();
        assertNotNull(PurchaseInvoiceList);
    }

//    @Test
//    void testGetById() {
//        PurchaseInvoice createdPurchaseInvoice = service.createPurchaseInvoice(createTestPurchaseInvoice());
//
//        PurchaseInvoice PurchaseInvoice = service.getById(createdPurchaseInvoice.getId());
//
//        assertNotNull(PurchaseInvoice);
//        assertEquals(PurchaseInvoice.getId(), createdPurchaseInvoice.getId());
//        assertEquals(PurchaseInvoice.getQuantity(), createdPurchaseInvoice.getQuantity());
//        assertEquals(PurchaseInvoice.getCostPrice(), createdPurchaseInvoice.getCostPrice());
//        assertEquals(PurchaseInvoice.getSum(), createdPurchaseInvoice.getSum());
//    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Purchase invoice with id %s not found\"", uuid), exception.getMessage());
    }

//    @Test
//    void testCreatePurchaseInvoice() {
//        PurchaseInvoice testPurchaseInvoice = createTestPurchaseInvoice();
//
//        PurchaseInvoice createdPurchaseInvoice = service.createPurchaseInvoice(testPurchaseInvoice);
//
//        assertNotNull(testPurchaseInvoice);
//        assertEquals(testPurchaseInvoice.getId(), createdPurchaseInvoice.getId());
//        assertEquals(testPurchaseInvoice.getQuantity(), createdPurchaseInvoice.getQuantity());
//        assertEquals(testPurchaseInvoice.getCostPrice(), createdPurchaseInvoice.getCostPrice());
//        assertEquals(testPurchaseInvoice.getSum(), createdPurchaseInvoice.getSum());
//
//    }
//
//    @Test
//    void testUpdatePurchaseInvoice() {
//        PurchaseInvoice createdPurchaseInvoice = service.createPurchaseInvoice(createTestPurchaseInvoice());
//
//        PurchaseInvoice purchaseInvoiceToUpdate = new PurchaseInvoice(createdPurchaseInvoice).toBuilder()
//                .id(UUID.randomUUID())
//                .number(54)
//                .client(createTestClient())
//                .product(createTestProduct())
//                .quantity(21)
//                .costPrice(625)
//                .sum(563)
//                .build();
//
//        PurchaseInvoice updatedPurchaseInvoice = service.updatePurchaseInvoice(createdPurchaseInvoice.getId(), purchaseInvoiceToUpdate);
//
//        assertEquals(createdPurchaseInvoice.getId(), updatedPurchaseInvoice.getId(), "Ids shouldn't be changed during the update");
//        assertNotEquals(updatedPurchaseInvoice.getNumber(), createdPurchaseInvoice.getNumber());
//        assertNotEquals(updatedPurchaseInvoice.getSum(), createdPurchaseInvoice.getSum());
//        assertNotEquals(updatedPurchaseInvoice.getQuantity(), createdPurchaseInvoice.getQuantity());
//        assertNotEquals(updatedPurchaseInvoice.getCostPrice(), createdPurchaseInvoice.getCostPrice());
//    }

//    @Test
//    void testDelete() {
//        PurchaseInvoice createdPurchaseInvoice = service.createPurchaseInvoice(createTestPurchaseInvoice());
//        UUID createdPurchaseInvoiceId = createdPurchaseInvoice.getId();
//        service.delete(createdPurchaseInvoiceId);
//
//        ResponseStatusException exception = assertThrows(
//                ResponseStatusException.class,
//                () -> service.getById(createdPurchaseInvoiceId),
//                format("Expected to return nothing and throw exception, since PurchaseInvoice with id %s was deleted",
//                        createdPurchaseInvoiceId)
//        );
//
//        assertEquals(format("404 NOT_FOUND \"purchaseInvoice with id %s not found\"", createdPurchaseInvoiceId), exception.getMessage());
//
//    }



    private PurchaseInvoice createTestPurchaseInvoice() {
        return PurchaseInvoice.builder()
                .number(5)
                .client(createTestClient())
                .product(createTestProduct())
                .quantity(2)
                .costPrice(65)
                .sum(56)
                .build();
    }

    private Product createTestProduct() {
        return Product.builder()
                .name("Eraser")
                .description("very good")
                .costPrice(5)
                .retailPrice(10)
                .quantity(4)
                .build();
    }

    private Client createTestClient() {
        return Client.builder()
                .firstName("Name")
                .lastName("Family")
                .phone("+79112105123")
                .email("builder@mail.ru")
                .build();
    }
}