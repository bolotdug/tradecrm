package university.innopolis.TradeCRM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import university.innopolis.TradeCRM.TradeCrmApplicationTests;
import university.innopolis.TradeCRM.models.PurchaseInvoice;
import university.innopolis.TradeCRM.models.SalesDocument;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SalesDocumentDatabaseServiceTest extends TradeCrmApplicationTests {

    @Autowired
    private SalesDocumentDatabaseService service;

    @Test
    void getAll() {
        List<SalesDocument> salesDocumentList = service.getAll();
        assertNotNull(salesDocumentList);
    }

    @Test
    void getById() {
    }

    @Test
    void createSalesDocument() {
    }

    @Test
    void updateSalesDocument() {
    }

    @Test
    void delete() {
    }
}