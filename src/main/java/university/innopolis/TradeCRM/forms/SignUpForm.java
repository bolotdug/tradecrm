package university.innopolis.TradeCRM.forms;

import lombok.Data;

import javax.persistence.Column;

@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
