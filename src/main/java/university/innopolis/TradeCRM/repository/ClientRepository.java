package university.innopolis.TradeCRM.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import university.innopolis.TradeCRM.models.Client;

import java.util.List;
import java.util.UUID;

@Repository
public interface ClientRepository extends CrudRepository<Client, UUID> {
}
