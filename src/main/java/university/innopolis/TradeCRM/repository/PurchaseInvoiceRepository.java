package university.innopolis.TradeCRM.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import university.innopolis.TradeCRM.models.PurchaseInvoice;

import java.util.UUID;

@Repository
public interface PurchaseInvoiceRepository extends CrudRepository<PurchaseInvoice, UUID>{
}
