package university.innopolis.TradeCRM.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import university.innopolis.TradeCRM.models.Product;

import java.util.UUID;

@Repository
public interface ProductRepository extends CrudRepository<Product, UUID> {
}
