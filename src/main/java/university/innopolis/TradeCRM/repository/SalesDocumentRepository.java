package university.innopolis.TradeCRM.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import university.innopolis.TradeCRM.models.SalesDocument;

import java.util.UUID;

@Repository
public interface SalesDocumentRepository extends CrudRepository<SalesDocument, UUID> {
}
