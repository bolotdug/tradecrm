package university.innopolis.TradeCRM.models;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "id")
@Entity
public class PurchaseInvoice {

    @Id
    private UUID id;

    private Integer number;
    private Integer quantity;
    private Integer costPrice;
    private Integer sum;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public PurchaseInvoice(PurchaseInvoice pi) {
        this.id = pi.id;
        this.number = pi.number;
        this.quantity = pi.quantity;
        this.costPrice = pi.costPrice;
        this.sum = pi.sum;
        this.client = pi.client;
        this.product = pi.product;
    }
}
