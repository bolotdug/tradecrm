package university.innopolis.TradeCRM.models;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "id")
@Entity
public class SalesDocument {

    @Id
    private UUID id;

    private Integer number;
    private Integer quantity;
    private Integer retailPrice;
    private Integer sum;


    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public SalesDocument(SalesDocument pi) {
        this.id = pi.id;
        this.number = pi.number;
        this.quantity = pi.quantity;
        this.retailPrice = pi.retailPrice;
        this.sum = pi.sum;
        this.product = pi.product;
    }
}
