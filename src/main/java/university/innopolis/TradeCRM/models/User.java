package university.innopolis.TradeCRM.models;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="usr")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder (toBuilder = true)
@EqualsAndHashCode(of = "id")
public class User {

    public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Id
    private UUID id;

    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String email;

    private String hashPassword;

    public User(User user) {
        this.role = user.role;
        this.id = user.id;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.email = user.email;
        this.hashPassword = user.hashPassword;
    }
}
