package university.innopolis.TradeCRM.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "id")
public class Product {

    @Id
    private UUID id;

    private String name;
    private String description;
    private String vendorName;
    private Integer retailPrice;
    private Integer costPrice;
    private Integer quantity;

    @OneToMany(mappedBy = "product")
    private List<PurchaseInvoice> purchaseInvoices;

    @OneToMany(mappedBy = "product")
    private List<SalesDocument> salesDocuments;

    public Product(Product product) {
        this.id = product.id;
        this.name = product.name;
        this.description = product.description;
        this.vendorName = product.vendorName;
        this.retailPrice = product.retailPrice;
        this.costPrice = product.costPrice;
        this.quantity = product.quantity;
        this.purchaseInvoices = product.purchaseInvoices;
        this.salesDocuments = product.salesDocuments;
    }
}
