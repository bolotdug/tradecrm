package university.innopolis.TradeCRM.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "client")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder (toBuilder = true)
@EqualsAndHashCode(of = "id")
public class Client {

    @Id
    private UUID id;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    @OneToMany(mappedBy = "client")
    private List<PurchaseInvoice> purchaseInvoiceList;

    public Client(Client client) {
        this.id = client.getId();
        this.firstName = client.getFirstName();
        this.lastName = client.getLastName();
        this.email = client.getEmail();
        this.phone = client.getPhone();
    }
}
