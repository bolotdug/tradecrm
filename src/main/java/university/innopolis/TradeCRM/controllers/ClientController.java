package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.Client;
import university.innopolis.TradeCRM.service.ClientService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @GetMapping
    public List<Client> getAllClients() {
        return clientService.getAll();
    }

    @GetMapping("/{id}")
    public Client getById(@PathVariable UUID id){
        return clientService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable UUID id){
        clientService.delete(id);
    }

    @PutMapping("/{id}")
    public Client updateById(@PathVariable UUID id, @RequestBody Client client){
        return clientService.updateClient(id, client);
    }
}
