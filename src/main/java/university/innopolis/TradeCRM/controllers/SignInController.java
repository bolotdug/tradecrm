package university.innopolis.TradeCRM.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SignInController {

    @GetMapping
    public String getSignInPage() {
        return "/registration/signIn";
    }

    @GetMapping("/signIn")
    public String getSignIn() {
        return "/registration/signIn";
    }

    @GetMapping("/index")
    public String getIndexPage() {
        return "index";
    }
}
