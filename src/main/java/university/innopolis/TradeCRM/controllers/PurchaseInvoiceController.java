package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.PurchaseInvoice;
import university.innopolis.TradeCRM.service.ClientService;
import university.innopolis.TradeCRM.service.ProductService;
import university.innopolis.TradeCRM.service.PurchaseInvoiceService;

import java.util.UUID;

@Controller
@RequestMapping("/ui/purchaseInvoices")
@RequiredArgsConstructor
public class PurchaseInvoiceController {

    private final PurchaseInvoiceService purchaseInvoiceService;
    private final ClientService clientService;
    private final ProductService productService;

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("purchaseInvoices", purchaseInvoiceService.getAll());
        return "purchaseInvoices/purchaseInvoices";
    }

    @GetMapping("/{id}")
    public String getpurchaseInvoice(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("products", productService.getAll());
        model.addAttribute("purchaseInvoice", purchaseInvoiceService.getById(id));
        return "purchaseInvoices/purchaseInvoice";
    }

    @PostMapping("/delete/{id}")
    public String deletepurchaseInvoice(@PathVariable UUID id){
        purchaseInvoiceService.delete(id);
        return "redirect:/ui/purchaseInvoices";
    }

    @PostMapping
    public String createOrUpdatepurchaseInvoice(@ModelAttribute("purchaseInvoiceDto") PurchaseInvoice purchaseInvoiceDto) {
        if (purchaseInvoiceDto.getId() == null) {
            purchaseInvoiceService.createPurchaseInvoice(purchaseInvoiceDto);
        } else {
            purchaseInvoiceService.updatePurchaseInvoice(purchaseInvoiceDto.getId(), purchaseInvoiceDto);
        }
        return "redirect:/ui/purchaseInvoices";
    }

    @GetMapping("/new")
    public String createPurchaseInvoice (Model model) {
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("products", productService.getAll());
        model.addAttribute("purchaseInvoice", new PurchaseInvoice());
        return "purchaseInvoices/purchaseInvoice";
    }
}
