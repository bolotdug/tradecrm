package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.service.ProductService;

import java.util.UUID;

@Controller
@RequestMapping("/ui/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public String getAll(Model model){

        model.addAttribute("products", productService.getAll());
        return "products/products";
    }

    @GetMapping("/{id}")
    public String getProduct(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("product", productService.getById(id));
        return "products/product";
    }

    @PostMapping("/delete/{id}")
    public String deleteProduct(@PathVariable UUID id){
        productService.delete(id);
        return "redirect:/ui/products";
    }

    @PostMapping
    public String createOrUpdateProduct(@ModelAttribute("productDto") Product productDto) {
        if (productDto.getId() == null) {
            productService.createProduct(productDto);
        } else {
            productService.updateProduct(productDto.getId(), productDto);
        }
        return "redirect:/ui/products";
    }

    @GetMapping("/new")
    public String createProduct (Model model) {
        model.addAttribute("product", new Product());
        return "products/product";
    }
}
