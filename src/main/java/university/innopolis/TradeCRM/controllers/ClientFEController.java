package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.Client;
import university.innopolis.TradeCRM.service.ClientService;

import java.util.UUID;

@Controller
@RequestMapping("/ui/clients")
@RequiredArgsConstructor
public class ClientFEController {

    private final ClientService clientService;

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("clients", clientService.getAll());
        return "clients/clients";
    }

    @GetMapping("/{id}")
    public String getClient(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("client", clientService.getById(id));
        return "clients/client";
    }

    @PostMapping("/delete/{id}")
    public String deleteClient(@PathVariable UUID id){
        clientService.delete(id);
        return "redirect:/ui/clients";
    }

    @PostMapping
    public String createOrUpdateClient(@ModelAttribute("clientDto") Client clientDto) {
        if (clientDto.getId() == null) {
            clientService.createClient(clientDto);
        } else {
            clientService.updateClient(clientDto.getId(), clientDto);
        }
        return "redirect:/ui/clients";
    }

    @GetMapping("/new")
    public String createClient (Model model) {
        model.addAttribute("client", new Client());
        return "clients/client";
    }
}
