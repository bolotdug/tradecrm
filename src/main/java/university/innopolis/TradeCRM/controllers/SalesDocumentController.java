package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.SalesDocument;
import university.innopolis.TradeCRM.service.ClientService;
import university.innopolis.TradeCRM.service.ProductService;
import university.innopolis.TradeCRM.service.SalesDocumentService;

import java.util.UUID;

@Controller
@RequestMapping("/ui/salesDocuments")
@RequiredArgsConstructor
public class SalesDocumentController {

    private final SalesDocumentService salesDocumentService;
    private final ClientService clientService;
    private final ProductService productService;

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("salesDocuments", salesDocumentService.getAll());
        return "salesDocuments/salesDocuments";
    }

    @GetMapping("/{id}")
    public String getsalesDocument(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("products", productService.getAll());
        model.addAttribute("salesDocument", salesDocumentService.getById(id));
        return "salesDocuments/salesDocument";
    }

    @PostMapping("/delete/{id}")
    public String deletesalesDocument(@PathVariable UUID id){
        salesDocumentService.delete(id);
        return "redirect:/ui/salesDocuments";
    }

    @PostMapping
    public String createOrUpdatesalesDocument(@ModelAttribute("salesDocumentDto") SalesDocument salesDocumentDto) {
        if (salesDocumentDto.getId() == null) {
            salesDocumentService.createSalesDocument(salesDocumentDto);
        } else {
            salesDocumentService.updateSalesDocument(salesDocumentDto.getId(), salesDocumentDto);
        }
        return "redirect:/ui/salesDocuments";
    }

    @GetMapping("/new")
    public String createsalesDocument (Model model) {
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("products", productService.getAll());
        model.addAttribute("salesDocument", new SalesDocument());
        return "salesDocuments/salesDocument";
    }
}
