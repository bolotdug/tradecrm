package university.innopolis.TradeCRM.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import university.innopolis.TradeCRM.models.User;
import university.innopolis.TradeCRM.service.UserService;

import java.util.UUID;

@Controller
@RequestMapping("/ui/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("users", userService.getAll());
        return "users/users";
    }

    @GetMapping("/{id}")
    public String getUser(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("user", userService.getById(id));
        return "users/user";
    }

    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable UUID id){
        userService.delete(id);
        return "redirect:/ui/users";
    }

    @PostMapping
    public String createOrUpdateUser(@ModelAttribute("userDto") User userDto) {
        if (userDto.getId() == null) {
            userService.createUser(userDto);
        } else {
            userService.updateUser(userDto.getId(), userDto);
        }
        return "redirect:/ui/users";
    }

    @GetMapping("/new")
    public String createUser (Model model) {
        model.addAttribute("user", new User());
        return "users/user";
    }
}
