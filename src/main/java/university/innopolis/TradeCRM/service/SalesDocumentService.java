package university.innopolis.TradeCRM.service;

import org.springframework.data.repository.CrudRepository;
import university.innopolis.TradeCRM.models.SalesDocument;
import university.innopolis.TradeCRM.models.SalesDocument;

import java.util.List;
import java.util.UUID;

public interface SalesDocumentService {
    List<SalesDocument> getAll();
    SalesDocument getById(UUID id);
    SalesDocument createSalesDocument(SalesDocument salesDocument);
    SalesDocument updateSalesDocument(UUID id, SalesDocument salesDocument);
    void delete(UUID id);
}
