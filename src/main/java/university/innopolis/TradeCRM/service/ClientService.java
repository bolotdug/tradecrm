package university.innopolis.TradeCRM.service;

import university.innopolis.TradeCRM.models.Client;

import java.util.List;
import java.util.UUID;

public interface ClientService {
    List<Client> getAll();
    Client getById(UUID id);
    Client createClient(Client client);
    Client updateClient(UUID id, Client client);
    void delete(UUID id);
}
