package university.innopolis.TradeCRM.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.repository.ProductRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ProductDatabaseService implements ProductService {
    private final ProductRepository productRepository;

    @PostConstruct
    public void init() {
        long productsCount = productRepository.count();
        if (productsCount < 4) {
            for (int i = 1; i < 4; i++) {
                Product product = Product.builder()
                        .id(UUID.randomUUID())
                        .name("Product" + i)
                        .description("Description of " + i)
                        .vendorName("vendor name " + i + "-665")
                        .retailPrice(i +1)
                        .costPrice(i + 1)
                        .quantity(i + 12)
                        .build();

                productRepository.save(product);
            }
        }
    }

    @Override
    public List<Product> getAll() {
        Iterable<Product> productIterable = productRepository.findAll();
        List<Product> productsList = new ArrayList<>();
        productIterable.forEach(productsList::add);
        return productsList;
    }

    @Override
    public Product getById(UUID id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("product with id %s not found", id)));
    }

    @Override
    public Product createProduct(Product product) {
        product.setId(UUID.randomUUID());
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(UUID id, Product product) {
        Product productFromDatabase = getById(id);

        Product productToUpdate = productFromDatabase.toBuilder()
                .name(product.getName())
                .description(product.getDescription())
                .vendorName(product.getVendorName())
                .retailPrice(product.getRetailPrice())
                .costPrice(product.getCostPrice())
                .quantity(product.getQuantity())
                .build();

        return productRepository.save(productToUpdate);
    }

    @Override
    public void delete(UUID id) {
        productRepository.deleteById(id);
    }
}
