package university.innopolis.TradeCRM.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.models.Client;
import university.innopolis.TradeCRM.repository.ClientRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ClientDatabaseService implements ClientService {

    private final ClientRepository clientRepository;

    @PostConstruct
    public void init() {
        long clientsCount = clientRepository.count();
        if (clientsCount < 10) {
            for (int i = 1; i < 10; i++) {
                Client client = Client.builder()
                        .id(UUID.randomUUID())
                        .firstName("First name" + i)
                        .lastName("Last name " + i)
                        .email("email" + i + "@test.ru")
                        .phone("+99999" + i)
                        .build();

                clientRepository.save(client);
            }
        }
    }

    @Override
    public List<Client> getAll() {
        Iterable<Client> clientIterable = clientRepository.findAll();
        List<Client> clientsList = new ArrayList<>();
        clientIterable.forEach(clientsList::add);
        return clientsList;
    }

    @Override
    public Client getById(UUID id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Client with id %s not found", id)));
    }

    @Override
    public Client createClient(Client client) {
        client.setId(UUID.randomUUID());
        return clientRepository.save(client);
    }

    @Override
    public Client updateClient(UUID id, Client client) {
        Client clientFromDatabase = getById(id);

        Client clientToUpdate = clientFromDatabase.toBuilder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .phone(client.getPhone())
                .build();

        return clientRepository.save(clientToUpdate);
    }

    @Override
    public void delete(UUID id) {
        clientRepository.deleteById(id);
    }

}
