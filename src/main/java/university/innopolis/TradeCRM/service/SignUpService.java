package university.innopolis.TradeCRM.service;

import university.innopolis.TradeCRM.forms.SignUpForm;

public interface SignUpService {
    void signUpUser(SignUpForm form);
}
