package university.innopolis.TradeCRM.service;

import university.innopolis.TradeCRM.models.Product;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    List<Product> getAll();
    Product getById(UUID id);
    Product createProduct(Product product);
    Product updateProduct(UUID id, Product product);
    void delete(UUID id);
}
