package university.innopolis.TradeCRM.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.models.PurchaseInvoice;
import university.innopolis.TradeCRM.repository.ProductRepository;
import university.innopolis.TradeCRM.repository.PurchaseInvoiceRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class PurchaseInvoiceDatabaseService implements PurchaseInvoiceService {

    private final PurchaseInvoiceRepository purchaseInvoiceRepository;
    private final ProductRepository productRepository;

    @Override
    public List<PurchaseInvoice> getAll() {
        Iterable<PurchaseInvoice> purchaseInvoiceIterable = purchaseInvoiceRepository.findAll();
        List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<>();
        purchaseInvoiceIterable.forEach(purchaseInvoiceList::add);
        return purchaseInvoiceList;
    }

    @Override
    public PurchaseInvoice getById(UUID id) {
        return purchaseInvoiceRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                format("Purchase invoice with id %s not found", id)));
    }

    @Override
    public PurchaseInvoice createPurchaseInvoice(PurchaseInvoice purchaseInvoice) {
        purchaseInvoice.setId(UUID.randomUUID());
        updateProductByPurchaseInvoice(purchaseInvoice, true);
        return purchaseInvoiceRepository.save(purchaseInvoice);
    }

    private void updateProductByPurchaseInvoice(PurchaseInvoice purchaseInvoice, Boolean invoiceAlreadyCreated) {
        UUID idOfProduct = purchaseInvoice.getProduct().getId();
        Product toUpdateProduct = productRepository.findById(idOfProduct)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("product with id %s not found", idOfProduct)));
        toUpdateProduct.setCostPrice(purchaseInvoice.getCostPrice());
        if (invoiceAlreadyCreated) {
            toUpdateProduct.setQuantity(toUpdateProduct.getQuantity() + purchaseInvoice.getQuantity());
        }
        productRepository.save(toUpdateProduct);
    }

    @Override
    public PurchaseInvoice updatePurchaseInvoice(UUID id, PurchaseInvoice purchaseInvoice) {
        PurchaseInvoice purchaseInvoiceFromDatabase = getById(id);

        PurchaseInvoice purchaseInvoiceToUpdate = purchaseInvoiceFromDatabase.toBuilder()
                .number(purchaseInvoice.getNumber())
                .client(purchaseInvoice.getClient())
                .product(purchaseInvoice.getProduct())
                .number(purchaseInvoice.getNumber())
                .costPrice(purchaseInvoice.getCostPrice())
                .sum(purchaseInvoice.getSum())
                .build();

        updateProductByPurchaseInvoice(purchaseInvoice, false);
        return purchaseInvoiceRepository.save(purchaseInvoiceToUpdate);
    }

    @Override
    public void delete(UUID id) {
        purchaseInvoiceRepository.deleteById(id);
    }
}
