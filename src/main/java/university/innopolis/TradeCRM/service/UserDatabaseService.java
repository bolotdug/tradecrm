package university.innopolis.TradeCRM.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.models.User;
import university.innopolis.TradeCRM.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class UserDatabaseService implements UserService {

    private final UserRepository userRepository;


    @Override
    public List<User> getAll() {
        Iterable<User> userIterable = userRepository.findAll();
        List<User> usersList = new ArrayList<>();
        userIterable.forEach(usersList::add);
        return usersList;
    }

    @Override
    public User getById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("user with id %s not found", id)));
    }

    @Override
    public User createUser(User user) {
        user.setId(UUID.randomUUID());
        return userRepository.save(user);
    }

    @Override
    public User updateUser(UUID id, User user) {
        User userFromDatabase = getById(id);

        User userToUpdate = userFromDatabase.toBuilder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();

        return userRepository.save(userToUpdate);
    }

    @Override
    public void delete(UUID id) {
        userRepository.deleteById(id);
    }

}
