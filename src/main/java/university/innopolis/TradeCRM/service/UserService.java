package university.innopolis.TradeCRM.service;

import university.innopolis.TradeCRM.models.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    List<User> getAll();
    User getById(UUID id);
    User createUser(User user);
    User updateUser(UUID id, User user);
    void delete(UUID id);
}
