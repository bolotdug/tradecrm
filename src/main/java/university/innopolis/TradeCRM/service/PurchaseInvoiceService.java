package university.innopolis.TradeCRM.service;

import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.models.PurchaseInvoice;

import java.util.List;
import java.util.UUID;

public interface PurchaseInvoiceService {
    List<PurchaseInvoice> getAll();
    PurchaseInvoice getById(UUID id);
    PurchaseInvoice createPurchaseInvoice(PurchaseInvoice purchaseInvoice);
    PurchaseInvoice updatePurchaseInvoice(UUID id, PurchaseInvoice purchaseInvoice);
    void delete(UUID id);
}
