package university.innopolis.TradeCRM.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import university.innopolis.TradeCRM.models.Product;
import university.innopolis.TradeCRM.models.SalesDocument;
import university.innopolis.TradeCRM.repository.ProductRepository;
import university.innopolis.TradeCRM.repository.SalesDocumentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class SalesDocumentDatabaseService implements SalesDocumentService {

    private final SalesDocumentRepository salesDocumentRepository;
    private final ProductRepository productRepository;

    @Override
    public List<SalesDocument> getAll() {
        Iterable<SalesDocument> salesDocumentIterable = salesDocumentRepository.findAll();
        List<SalesDocument> salesDocumentList = new ArrayList<>();
        salesDocumentIterable.forEach(salesDocumentList::add);
        return salesDocumentList;
    }

    @Override
    public SalesDocument getById(UUID id) {
        return salesDocumentRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                format("Purchase invoice with id %s not found", id)));
    }

    @Override
    public SalesDocument createSalesDocument(SalesDocument salesDocument) {
        salesDocument.setId(UUID.randomUUID());
        updateProductBysalesDocument(salesDocument, true);
        return salesDocumentRepository.save(salesDocument);
    }

    private void updateProductBysalesDocument(SalesDocument salesDocument, Boolean invoiceAlreadyCreated) {
        UUID idOfProduct = salesDocument.getProduct().getId();
        Product toUpdateProduct = productRepository.findById(idOfProduct)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("product with id %s not found", idOfProduct)));
        toUpdateProduct.setRetailPrice(salesDocument.getRetailPrice());
        if (invoiceAlreadyCreated) {
            toUpdateProduct.setQuantity(toUpdateProduct.getQuantity() - salesDocument.getQuantity());
        }
        productRepository.save(toUpdateProduct);
    }

    @Override
    public SalesDocument updateSalesDocument(UUID id, SalesDocument salesDocument) {
        SalesDocument salesDocumentFromDatabase = getById(id);

        SalesDocument salesDocumentToUpdate = salesDocumentFromDatabase.toBuilder()
                .number(salesDocument.getNumber())
                .retailPrice(salesDocument.getRetailPrice())
                .product(salesDocument.getProduct())
                .quantity(salesDocument.getQuantity())
                .sum(salesDocument.getSum())
                .build();

        updateProductBysalesDocument(salesDocument, false);
        return salesDocumentRepository.save(salesDocumentToUpdate);
    }

    @Override
    public void delete(UUID id) {
        salesDocumentRepository.deleteById(id);
    }
}
